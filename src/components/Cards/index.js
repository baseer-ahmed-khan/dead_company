import React from "react";
import styles from "./card.module.scss";
import { Row } from "react-bootstrap";

export const CategoryCard = () => {
  return (
    <div className={styles.catg_card}>
      <Row className={styles.row}>
        <div className={styles.catg}>
          <p className={styles.title}>Name</p>
          <p className={styles.desc}>The Dead Collection</p>
        </div>
        <div className={styles.catg}>
          <p className={styles.title}>Supply</p>
          <p className={styles.desc}>3,333</p>
        </div>
        <div className={styles.catg}>
          <p className={styles.title}>WL</p>
          <p className={styles.desc}>1,500</p>
        </div>
        <div className={styles.catg}>
          <p className={styles.title}>MINT</p>
          <p className={styles.desc}>TBA</p>
        </div>
        <div className={styles.catg}>
          <p className={styles.title}>Date</p>
          <p className={styles.desc}>TBA</p>
        </div>
      </Row>
    </div>
  );
};
