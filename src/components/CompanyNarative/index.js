import React,{useEffect, useState} from "react";
import rightArrow from '../../assets/images/icons/ArrowRight.svg'
import styles from "./companyNarative.module.scss";





const CompanyNarative = () => {
  // const onlyWidth =  useWindowWidth()
  const size = useWindowSize();



const NarativeDesktop = [
  {
    type:'Chapter',
    number:'1',
    chapterName:'3045',
  },
  {
    chapterName:'3045',
    type:'slide',
    desc:[
      ` In the year 3045, Axel and Cody embarked on a quest to retrieve
      the Book Of The Dead- an ancient scripture believed to contain
      hidden secrets of a tribe buried in the secret underground
      tunnels of Zulenka- once known as Los Angeles.`,
      `Their research led them to the mountains of Peru, the only place
      on earth that was yet to be infected with technology.`,
    ]
  },
  {
    type:'Chapter',
    number:'2',
    chapterName:'03:20 AM',
  },
  ...(size.width <527 ?[
  {
    chapterName:'03:20 AM',
    type:'slide',
    desc:[
      `Do you hear that? Axel asked Cody in a state of panic. ‘No, what was it?’ Cody replied.`,
      `I heard footsteps outside our tent’ Axel explained. ‘That can't be, we’re here in the mountains in the middle of nowhere and we haven’t seen a single person in 2 days, it was probably the wind, go back to sleep, we have a long hike tomorrow.’ said Cody in a reassuring yet simultaneously uneasy tone.`,
     
    ]
  },
  {
    chapterName:'03:20 AM',
    type:'slide',
    desc:[
      `It's not people I'm worried about, there’s wolves in these mountains....`,
      `Fine, let's go out and check, grab the torch and blade`
    ]
  },
]:[

  {
    chapterName:'03:20 AM',
    type:'slide',
    desc:[
      `Do you hear that? Axel asked Cody in a state of panic. ‘No, what was it?’ Cody replied.`,
      `I heard footsteps outside our tent’ Axel explained. ‘That can't be, we’re here in the mountains in the middle of nowhere and we haven’t seen a single person in 2 days, it was probably the wind, go back to sleep, we have a long hike tomorrow.’ said Cody in a reassuring yet simultaneously uneasy tone.`,
      `It's not people I'm worried about, there’s wolves in these mountains....`,
      `Fine, let's go out and check, grab the torch and blade`
    ]
  },

  ]),

  {
    type:'Chapter',
    number:'3',
    chapterName:'Destiny',
  },

  ...(size.width <527 ?[
    {
      chapterName:'Destiny',
      type:'slide',
      desc:[
       `As the pair slowly unzipped the tent door and proceeded to exit their temporary home, they were greeted by a tall hooded figure. ‘Axel and Cody, I have been waiting for you, you have many questions, and although the process has altered your consciousness, you remain irrevocably human’ said the old man. ‘Who are you? And how do you know our name?!’ cried Axel. ‘I am that which you were seeking, I possess the Book Of The Dead’`,
       `The boys glanced at each other in a complete state of shock.`,
       `‘Well, can we see it? What does it say inside?’     `,
      ]
    },
    {
      chapterName:'Destiny',
      type:'slide',
      desc:[
       `‘I don't know, I haven’t opened it, no one can except you, Axel’`,
       `‘What do you mean?’ Axel asked.`,
       `‘The book can only be opened by a descendant of the buried tribe, you Axel, are a direct descendant of Mansa, the king of the tribe. Your quest for this book, the very reason you are here, is no mere accident, this was your destiny.’ 
       The old hooded man vanished into thin air as Axel and Cody froze to the ground waiting to awaken from what they believed was a dream- ironically, that wasn’t far from the truth`,
      ]
    },
    {
      chapterName:'Destiny',
      type:'slide',
      desc:[
     
      `As they reluctantly made their way back into the tent, they noticed a glowing object on the ground, exactly where the man was standing. They approached with caution, quickly realising it was a book, the book.`,
      `‘Open it Axel, you heard what the man said’`,
      `‘No, let's wait until we fly back home to Zulenka, I don't trust this place.`
      ]
    },

  ]:[
    {
      chapterName:'Destiny',
      type:'slide',
      desc:[
       `As the pair slowly unzipped the tent door and proceeded to exit their temporary home, they were greeted by a tall hooded figure. ‘Axel and Cody, I have been waiting for you, you have many questions, and although the process has altered your consciousness, you remain irrevocably human’ said the old man. ‘Who are you? And how do you know our name?!’ cried Axel. ‘I am that which you were seeking, I possess the Book Of The Dead’`,
       `The boys glanced at each other in a complete state of shock.`,
       `‘Well, can we see it? What does it say inside?’     `,
       `‘I don't know, I haven’t opened it, no one can except you, Axel’`,
       `‘What do you mean?’ Axel asked.`
      ]
    },
    {
      chapterName:'Destiny',
      type:'slide',
      desc:[
      `‘The book can only be opened by a descendant of the buried tribe, you Axel, are a direct descendant of Mansa, the king of the tribe. Your quest for this book, the very reason you are here, is no mere accident, this was your destiny.’ 
      The old hooded man vanished into thin air as Axel and Cody froze to the ground waiting to awaken from what they believed was a dream- ironically, that wasn’t far from the truth`,
      `As they reluctantly made their way back into the tent, they noticed a glowing object on the ground, exactly where the man was standing. They approached with caution, quickly realising it was a book, the book.`,
      `‘Open it Axel, you heard what the man said’`,
      `‘No, let's wait until we fly back home to Zulenka, I don't trust this place.`
      ]
    },
  ]
  ),
  
  {
    type:'Chapter',
    number:'4',
    chapterName:'The Awakening',
  },
  {
    chapterName:'The Awakening',
    type:'slide',
    desc:[
   `It had been 8 hours since Axel and Cody landed home, they were now sitting at their local cafe with the Book Of The Dead on the table ready for Axel to open.`,
   `‘Let's do this’ Cody encouraged.`,
   `Axel proceeded to open the book to the first page- Blank. Second page, blank. Then finally on the third page, they were met by what appeared to be scriptures in Hebrew with a mixture of numbers and Arabic letters. `,
   `Suddenly, the ground beneath them started violently shaking before a loud sound of a trumpet swallowed the city. `,
   `Everyone in sight was visibly struck with an undeniable realisation of their mortality.`
    ]
  },


  {
    type:'Chapter',
    number:'5',
    chapterName:'The Resurrection',
  },
  ...(size.width <527 ?[
    {
      chapterName:'The Resurrection',
      type:'slide',
      desc:[
     `‘Let's get out of here’ Axel yelled whilst quickly closing the book and beaming towards the exit. `,
     `‘Start the car, go go go!’ Cody exclaimed.`,
     `‘It wont start!’ Axel panicked.`,
     `The two noticed they were no longer alone in the car, as a figure was noticeably sitting in the back seat.`,
     
     
  
      ]
    },
    {
      chapterName:'The Resurrection',
      type:'slide',
      desc:[
        `‘It seems as though fate has reunited us’ the old hooded man said with a smile.`,
        `‘You again? Look what you made us do! What even is this book?!’ Shouted Axel.`,
        `‘You see, every programme has a purpose, if it does not, it is deleted- today Axel, you have fulfilled your purpose, you have awoken the dead, but now, you must be deleted’ said the old man as he placed the palm of his hand on the back of Axel’s head causing him to instantly evaporate.`,
        `‘Please don't hurt me, please!’ Cody yelled`,
        `‘I won't, but I cannot speak for them’ said the old man whilst looking towards the sidewalk’`,
      ]
    },
    {
      chapterName:'The Resurrection',
      type:'slide',
      desc:[
         
          `‘Who’s them?’ Cody asked`,
          `‘See for yourself’ the old man pointed.`,
          `The side-walks began to crack and break by what appeared to be hands and arms that were forcefully piercing the ground from beneath. `,
          `The Dead were resurrected.`
      ]
    },

  ]:
  [
    {
      chapterName:'The resurrection',
      type:'slide',
      desc:[
     `‘Let's get out of here’ Axel yelled whilst quickly closing the book and beaming towards the exit. `,
     `‘Start the car, go go go!’ Cody exclaimed.`,
     `‘It wont start!’ Axel panicked.`,
     `The two noticed they were no longer alone in the car, as a figure was noticeably sitting in the back seat.`,
     `‘It seems as though fate has reunited us’ the old hooded man said with a smile.`,
     `‘You again? Look what you made us do! What even is this book?!’ Shouted Axel.`
  
      ]
    },
    {
      chapterName:'The resurrection',
      type:'slide',
      desc:[
          `‘You see, every programme has a purpose, if it does not, it is deleted- today Axel, you have fulfilled your purpose, you have awoken the dead, but now, you must be deleted’ said the old man as he placed the palm of his hand on the back of Axel’s head causing him to instantly evaporate.`,
          `‘Please don't hurt me, please!’ Cody yelled`,
          `‘I won't, but I cannot speak for them’ said the old man whilst looking towards the sidewalk’`,
          `‘Who’s them?’ Cody asked`,
          `‘See for yourself’ the old man pointed.`,
          `The side-walks began to crack and break by what appeared to be hands and arms that were forcefully piercing the ground from beneath. `,
          `The Dead were resurrected.`
      ]
    },
  ]
  ),
  

  {
    type:'Chapter',
    number:'',
    chapterName:'THE END',
  },
   
]




  return (
    <div className={styles.narative}>
      <h2>Once upon a time..</h2>

      <div className={styles.scrollBox}>


      {NarativeDesktop.map((data)=>(
        
        data.type === 'Chapter' ? 
        <div className={styles.slides}>
            <div className={styles.chapterPage}>
              <p className={styles.chapternumber}>{data.number == '' ? ' ' : data.type} {data.number}</p>
              <h1 className={styles.title}>{data.chapterName} </h1>
            </div>
            {data.chapterName !== 'THE END' ? 
            <div className={styles.pageArrow}> <img src={rightArrow} /> </div>
            : ''}
         </div>
        :
        <div className={styles.slides}>
          <div className={styles.page}>
            <div className={styles.chapterName}>{data.chapterName}</div>
            <div className={styles.desc}>
            {
              data.desc.map(item=>(
                  <p>{item}</p>
              ))
            }
            </div>
            <div className={styles.pageArrow}> <img src={rightArrow} /> </div>
          </div>
      </div>

        
      ))

      }



        
      </div>
    </div>
  );
};

export default CompanyNarative;



// Hook
function useWindowSize() {
  // Initialize state with undefined width/height so server and client renders match
  // Learn more here: https://joshwcomeau.com/react/the-perils-of-rehydration/
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
  });
  useEffect(() => {
    // Handler to call on window resize
    function handleResize() {
      // Set window width/height to state
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    }
    // Add event listener
    window.addEventListener("resize", handleResize);
    // Call handler right away so state gets updated with initial window size
    handleResize();
    // Remove event listener on cleanup
    return () => window.removeEventListener("resize", handleResize);
  }, []); // Empty array ensures that effect is only run on mount
  return windowSize;
}