import React,{useEffect} from "react";
import styles from "./layout.module.scss";
import { Container, Row } from "react-bootstrap";
import AppHeader from "../Header";
import AppFooter from "../Footer";
import classNames from "classnames";

const PageLayout = ({
  backgroundColor,
  className,
  BGclassName,
  banner,
  children,
  leftIcon,
  rightIcon,
  title,
  aboutdata,
  marginTop,
  padding,
  marketPlace,
  videourl,
  titleDivclassName
}) => {
useEffect(() => {
  if(document.getElementById('vid')){
    document.getElementById('vid').play();
  }
  
}, )

  return (
    <div style={{ backgroundColor }}>
      <Container className={styles.container}>
        <AppHeader />
          {title && (
            <div className={classNames(styles.title_div, titleDivclassName)}>
              <img src={title} />
            </div>
          )}
        <div className={classNames(styles.bgImgContainer , BGclassName)}>
          <div className={styles.row} style={{ marginTop, padding }}>
            {/* {leftIcon && (
              <div className={styles.icon}>
                <img className={styles.lightIcon} src={lightning} />
              </div>
            )} */}
            {banner && 
            <div className={classNames(styles.banner_div, className)}>
              <img src={banner} />
            </div>
            }
            {videourl &&
              <video id='vid'  className={styles.videoBox} src={videourl}  autoPlay="true" loop="true" muted="true" playsinline="true">
             </video>}
            {/* {rightIcon && (
              <div className={styles.icon}>
                <img className={styles.lightIcon} src={lightning} />
              </div>
            )} */}
            {aboutdata && (
              <div className={styles.AboutUsContainer}>

                  {aboutdata.map((data) => (
                    <>
                      <div className={styles.heading}>
                        {data[0]}
                            </div>
                <div className={styles.para}>
                            <p>{data[1]}</p>
                </div>
                            </>

                  ))}
              </div>
            )}
            {marketPlace}
          </div>
        </div>
        <AppFooter />
        <div>{children}</div>
      </Container>
    </div>
  );
  // data,i,row
  
  // (i + 1) === row.length ?<>  <p>{data}</p>  </> : <> <p>{data}</p> <br /> <br /> </>
};

export default PageLayout;
