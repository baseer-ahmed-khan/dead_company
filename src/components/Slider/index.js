import React from "react";
import { Carousel } from "react-bootstrap";

const AppSlider = () => {
  return (
    <div className="app-slider">
    <p className="slider-title">UTILITIES</p>
      <Carousel controls={false}>
        <Carousel.Item interval={10000}>
          <p className="heighlight">
            01.&nbsp;
            <span className="slider-desc">
              As part of Phase 2 of the Road map, we will be creating and
              building a 3D style art gallery/market place- think of it as a
              futuristic & visually superior version of ‘Opensea’. There, people
              will be able to list/browse/bid/buy/sell pieces of digital art,
              and host their own private exhibitions. All in an immersive life
              like 3d world. We charge a commission fee for each transaction
              that occurs on the platform, then, a % of that commission, will be
              equally & automatically distributed to ‘The Dead Collection’ NFT
              holders. This type of utility will be the first of its kind in the
              NFT space.
            </span>
          </p>
        </Carousel.Item>
        <Carousel.Item interval={3000}>
          <p className="heighlight">
            02.&nbsp;
            <span className="slider-desc">
            Jacob, our co-owner, is the founder & owner of www.nftflex.store, the worlds first NFT customisation store. Customers are able to order wallets, phone cases, rugs, & much more, of their chosen NFT’s. We then incorporate their NFT on their chosen asset, and deliver it to their home, so they can flex their NFT in real life, rather than have it sitting in a virtual wallet 24/7. Holders of ‘The Dead Collection’ will have a 30% discount on this store.
            </span>
          </p>
        </Carousel.Item>
        <Carousel.Item interval={3000}>
          <p className="heighlight">
            03.&nbsp;
            <span className="slider-desc">
            111 pieces of ‘The Dead Collection’ NFTs will contain a special attribute *TBA*. If you are lucky enough to own one, you are automatically whitelisted for our next project.
            </span>
          </p>
        </Carousel.Item>
        <Carousel.Item interval={3000}>
          <p className="heighlight">
            04.&nbsp;
            <span className="slider-desc">
            66 random holders will receive a limited edition ‘the dead company’ cap, with their unique NFT serial number on the side of it.
            </span>
          </p>
        </Carousel.Item>
        <Carousel.Item interval={3000}>
          <p className="heighlight">
            05.&nbsp;
            <span className="slider-desc">
             6 random holders will receive a real life figure of their NFT.
            </span>
          </p>
        </Carousel.Item>

        <Carousel.Item interval={3000}>
          <p className="heighlight">
            06.&nbsp;
            <span className="slider-desc">
            Last but certainly not least, holders will receive regular invitations to exclusive private parties all over the world, with influencers and famous music artists in attendance. The first will be held in summer 2022, London UK.
            </span>
          </p>
        </Carousel.Item>
      </Carousel>
    </div>
  );
};
export default AppSlider;
