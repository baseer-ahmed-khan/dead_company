import React from 'react'
import { BrowserRouter as Router, Routes, Route, useRoutes, Navigate } from "react-router-dom";
import Landing from '../screens/landing';
import HomeScreen from '../screens/home/home';
import CommingSoon from '../screens/commingSoon';
import AboutUs from '../screens/aboutUs/aboutUs';
import MarketPlace from '../screens/marketPlace/marketplace';
import Shop from '../screens/shop/shop';


const Navigation = () => {
    return (
        <Router>
            <Routes>
                
                <Route exact path="/comingsoon" element={<CommingSoon />} />
                <Route exact path="/aboutus" element={<AboutUs />} />
                <Route exact path="/marketplace" element={<MarketPlace />} />
                <Route exact path="/shop" element={<Shop />} />
                <Route exact path="/thedeadcompany" element={<Landing />} />
                <Route exact path="/" element={<HomeScreen />} />
            </Routes>
        </Router>
    )
}

export default Navigation;