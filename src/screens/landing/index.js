import React from "react";
import landingVideo from '../../assets/videos/NFTLoopLandingPage.mov';
import styles from "./landing.module.scss";
import {Container } from "react-bootstrap";
import PageLayout from "../../components/PageLayout";
import { CategoryCard } from "../../components/Cards";
import RoadMap from "../../components/RoadMap";
import AppSlider from "../../components/Slider";
import Team from "../../components/Team";
import FAQs from "../../components/Faqs";
import CompanyNarative from "../../components/CompanyNarative";
import deadcomapny from "../../assets/images/images/thedeadcollection.png";

const Landing = () => {
  return (
    <PageLayout className={styles.landingPageBanner}  leftIcon rightIcon title={deadcomapny} marginTop="0px" padding="20px 0" videourl={landingVideo} >
      <Container style={{marginTop: 50}}>
       <CategoryCard/>
       <CompanyNarative/>
       <RoadMap/>
       <AppSlider/>
       <Team/>
       <FAQs/>
      </Container>
    </PageLayout>
  );
};

export default Landing;
// 