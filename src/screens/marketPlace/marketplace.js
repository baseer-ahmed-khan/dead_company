import React from "react";
import PageLayout from "../../components/PageLayout";
import styles from "./style.module.scss";
import {MarketPlaceData} from '../../components/MarketPlaceData'
import commingsoonimage from '../../assets/images/images/comingsoon.png'

function MarketPlace() {
  return (
    <PageLayout
      backgroundColor="#000"
      title={commingsoonimage}
      BGclassName={styles.marketBox}
      marketPlace = {<MarketPlaceData/>}
    ></PageLayout>
  );
}

export default MarketPlace;
