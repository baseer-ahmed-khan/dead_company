import React from "react";
import PageLayout from "../../components/PageLayout";
import {ShopData} from '../../components/ShopData'
import commingsoonimage from '../../assets/images/images/comingsoon.png'
import styles from "./style.module.scss";

function Shop() {
  return (
    <PageLayout
      backgroundColor="#000"
      title={commingsoonimage}
      BGclassName={styles.shopBox}
      marketPlace = {<ShopData/>}
    ></PageLayout>
  );
}

export default Shop;
